package org.matomo.sdk.extra;

import ohos.app.Context;
import ohos.data.preferences.Preferences;
import ohos.system.version.SystemVersion;
import org.matomo.sdk.*;
import org.matomo.sdk.tools.Checksum;

import java.io.File;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DownloadTracker {//todo 这个类先写死
    protected static final String TAG = Matomo.tag(DownloadTracker.class);
    private static final String INSTALL_SOURCE_GOOGLE_PLAY = "com.android.vending";
    private final Tracker mTracker;
    private final Object mTrackOnceLock = new Object();
    private final Preferences mPreferences;
    private final Context mContext;
    private final boolean isInternalTracking;
    private String mVersion;

    public interface Extra {
        /**
         * Does your {@link Extra} implementation do work intensive stuff?
         * Network? IO?
         *
         * @return true if this should be run async and on a sepperate thread.
         */
        boolean isIntensiveWork();

        /**
         * Example:
         * <br>
         * com.example.pkg:1/ABCDEF01234567
         * <br>
         * "ABCDEF01234567" is the extra identifier here.
         *
         * @return a string that will be used as extra identifier or null
         */

        File buildExtraIdentifier();

        /**
         * The MD5 checksum of the apk file.
         * com.example.pkg:1/ABCDEF01234567
         */
        class ApkChecksum implements Extra {
            String entryDir;

            public ApkChecksum(Context context) {
                try {
                    entryDir = context.getApplicationInfo().getEntryDir();
                } catch (Exception e) {
                    entryDir = null;
                }
            }

            public ApkChecksum() {
            }

            @Override
            public boolean isIntensiveWork() {
                return true;
            }

            @Override
            public File buildExtraIdentifier() {
                if (entryDir != null) {
                    try {
                        return Checksum.encode(new File(entryDir));
                    } catch (Exception e) {
                    }
                }
                return null;
            }
        }

        /**
         * Custom exta identifier. Supply your own \o/.
         */
        abstract class Custom implements Extra {
        }

        /**
         * No extra identifier.
         * com.example.pkg:1
         */
        class None implements Extra {
            @Override
            public boolean isIntensiveWork() {
                return false;
            }

            @Override
            public File buildExtraIdentifier() {
                return null;
            }
        }
    }

    public DownloadTracker(Tracker tracker) {
        mTracker = tracker;
        mContext = tracker.getMatomo().getContext();
        mPreferences = tracker.getPreferences();
        isInternalTracking = true;
    }

    public void setVersion(String version) {
        mVersion = version;
    }

    public String getVersion() {
        if (mVersion != null) return mVersion;
        return Integer.toString(SystemVersion.getApiVersion());
    }

    public void trackOnce(TrackMe baseTrackme, Extra extra) {
        String firedKey = "downloaded:" + mContext.getBundleName() + ":" + getVersion();
        synchronized (mTrackOnceLock) {
            if (!mPreferences.getBoolean(firedKey, false)) {
                mPreferences.putBoolean(firedKey, true).flushSync();
                trackNewAppDownload(baseTrackme, extra);
            }
        }
    }

    public void trackNewAppDownload(final TrackMe baseTrackme, final Extra extra) {
        // We can only get referrer information if we are tracking our own app download.
        final boolean isDelay = isInternalTracking && INSTALL_SOURCE_GOOGLE_PLAY.equals(mContext.getBundleName());
        mContext.getUITaskDispatcher().asyncDispatch(new Runnable() {
            @Override
            public void run() {
                if (isDelay){
                    try {
                        Thread.sleep(3000);
                    } catch (Exception e) {
                    }
                }
                trackNewAppDownloadInternal(baseTrackme, extra);
            }
        });
    }

    private void trackNewAppDownloadInternal(TrackMe baseTrackMe, Extra extra) {
        StringBuilder installIdentifier = new StringBuilder();
        installIdentifier.append("http://").append(mContext.getBundleName()).append(":").append(getVersion());

        File extraIdentifier = extra.buildExtraIdentifier();
        if (extraIdentifier != null) installIdentifier.append("/").append(extraIdentifier);
        //todo 先写成空
        String referringApp = new PackageManagerImpl().getInstallerPackageName(PackageManagerImpl.TARGET_PACKAGE);

        if (referringApp != null && referringApp.length() > 200)
            referringApp = referringApp.substring(0, 200);

        if (referringApp != null && referringApp.equals(INSTALL_SOURCE_GOOGLE_PLAY)) {
            // For this type of install source we could have extra referral information
            String referrerExtras = mTracker.getMatomo().getPreferences().getString(InstallReferrerReceiver.PREF_KEY_INSTALL_REFERRER_EXTRAS, null);
            if (referrerExtras != null) referringApp = referringApp + "/?" + referrerExtras;
        }

        if (referringApp != null) referringApp = "http://" + referringApp;

        mTracker.track(baseTrackMe
                .set(QueryParams.EVENT_CATEGORY, "Application")
                .set(QueryParams.EVENT_ACTION, "downloaded")
                .set(QueryParams.ACTION_NAME, "application/downloaded")
                .set(QueryParams.URL_PATH, "/application/downloaded")
                .set(QueryParams.DOWNLOAD, installIdentifier.toString())
                .set(QueryParams.REFERRER, referringApp)); // Can be null in which case the TrackMe removes the REFERRER parameter.
    }
}
