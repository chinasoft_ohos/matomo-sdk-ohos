package org.matomo.sdk.tools;

public class PropertySource {
    /**
     * getHttpAgent
     *
     * @return String
     */
    public String getHttpAgent() {
        return getSystemProperty("http.agent");
    }

    /**
     * getJVMVersion
     *
     * @return String
     */
    public String getJVMVersion() {
        return getSystemProperty("java.vm.version");
    }

    /**
     * getSystemProperty
     *
     * @param key key
     * @return String
     */
    public String getSystemProperty(String key) {
        return System.getProperty(key);
    }
}
