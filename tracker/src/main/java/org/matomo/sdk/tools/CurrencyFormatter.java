/*
 * SDK for Matomo
 *
 */

package org.matomo.sdk.tools;

import java.math.BigDecimal;

public class CurrencyFormatter {
    public static String priceString(Integer cents) {
        if (cents == null) return null;
        return new BigDecimal(cents).movePointLeft(2).toPlainString();
    }
}
