package org.matomo.sdk.tools;

public class Objects {
    /**
     * equals
     *
     * @param a a
     * @param b b
     * @return boolean
     */
    public static boolean equals(Object a, Object b) {
        return (a == b) || (a != null && a.equals(b));
    }
}
