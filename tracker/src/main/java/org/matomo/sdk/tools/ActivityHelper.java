package org.matomo.sdk.tools;

import ohos.aafwk.ability.Ability;

import java.util.ArrayList;
import java.util.List;

public class ActivityHelper {
    public static String getBreadcrumbs(final Ability activity) {
        Ability ability = activity;
        ArrayList<String> breadcrumbs = new ArrayList<>();
        breadcrumbs.add(ability.getAbilityInfo().getLabel());
        return joinSlash(breadcrumbs);
    }

    private static String joinSlash(List<String> sequence) {
        if (sequence != null && sequence.size() > 0) {
            StringBuilder builder = new StringBuilder();
            for (int ii = 0; ii < sequence.size(); ii++) {
                if (ii == sequence.size() - 1) {
                    builder.append(sequence.get(ii));
                } else {
                    builder.append(sequence.get(ii)).append("/");
                }
            }
            return builder.toString();
        }
        return "";
    }

    public static String breadcrumbsToPath(String breadcrumbs) {
        return breadcrumbs.replaceAll("\\s", "");
    }
}
