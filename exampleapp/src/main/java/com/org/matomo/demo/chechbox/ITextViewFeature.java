/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.chechbox;

import ohos.agp.components.Component;

/**
 * TextView特性功能接口
 *
 * @author ZhongDaFeng
 */
public interface ITextViewFeature {
    public void setEnabled(boolean isEnabled);

    public void setSelected(boolean isSelected);

    public void onVisibilityChanged(Component changedView, int visibility);
}
