/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.org.matomo.demo.button;

import com.org.matomo.demo.chechbox.RHelper;
import com.org.matomo.demo.chechbox.RTextHelper;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * RTextView
 *
 * @author ZhongDaFeng
 */
public class RText extends Text implements RHelper<RTextHelper> {
    private RTextHelper mHelper;

    /**
     * RText
     *
     * @param context   context
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public RText(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    /**
     * RText
     *
     * @param context context
     */
    public RText(Context context) {
        this(context, null);
    }

    /**
     * RText
     *
     * @param context context
     * @param attrs   attrs
     */
    public RText(Context context, AttrSet attrs) {
        super(context, attrs);
        mHelper = new RTextHelper(context, this, attrs);
    }

    /**
     * RTextHelper
     *
     * @return RTextHelper
     */
    @Override
    public RTextHelper getHelper() {
        return mHelper;
    }

    /**
     * setEnabled
     *
     * @param isEnabled isEnabled
     */
    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (mHelper != null) mHelper.setEnabled(isEnabled);
    }

    /**
     * setSelected
     *
     * @param isSelected isSelected
     */
    @Override
    public void setSelected(boolean isSelected) {
        if (mHelper != null) mHelper.setSelected(isSelected);
        super.setSelected(isSelected);
    }

    /**
     * isTextCursorVisible
     *
     * @return boolean
     */
    @Override
    public boolean isTextCursorVisible() {
        return super.isTextCursorVisible();
    }

    /**
     * setVisibility
     *
     * @param visibility visibility
     */
    @Override
    public void setVisibility(int visibility) {
        if (mHelper != null) mHelper.onVisibilityChanged(null, visibility);
        super.setVisibility(visibility);
    }
}
